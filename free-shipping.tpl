<style>
    .progress {
        background-color: rgb(245, 245, 245);
        border-bottom-color: rgb(119, 119, 119);
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        height: 10px;
        width: 300px;
        padding: 3px;
    }

    .progress > span {
        content: '';
        display: block;
        background-color: #76A548;
        width: 80%;
        height: 100%;
    }

</style>
<div class="free-delivery-hook-content">
    {if isset($toFreeShippingPrice) && $toFreeShippingPrice > 0 && isset($freeShippingPrice)}
        <div style="width:300px">
            {if isset($productValue)}
                <span>
                    {l s='Product value:' mod='freedelivery'} {$productValueDisplay}
                </span>
            {/if}
            <span>
                {l s='To free shipping missing:' mod='freedelivery'}
            </span>
            <span>
                    {$toFreeShippingPriceDisplay}
            </span>
            {assign var=productsValue value=$freeShippingPrice-$toFreeShippingPrice|string_format:"%.2f"}
            {assign var=freeShippingLength value=$productsValue/$freeShippingPrice*100}
            {assign var=tofreeShipping value=$freeShippingPrice-$productsValue}

            <div class="progress">
                <span style="width: {$freeShippingLength}%"></span>
            </div>
        </div>
    {/if}
</div>