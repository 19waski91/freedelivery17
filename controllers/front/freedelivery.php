<?php

include_once (_PS_MODULE_DIR_.'freedelivery/freedelivery.php');

class FreeDeliveryFreeDeliveryModuleFrontController extends ModuleFrontController
{

    public function displayAjaxGetFreeDelivery()
    {
        $freeDelivery = ModuleCore::getInstanceByName('freedelivery');

        $tpl = $freeDelivery->getContentFreeShipping();

        $this->ajaxDie(Tools::jsonEncode(array('success' => 1
                                                , 'html' => $tpl, )));
    }
}
