<?php
class Hook extends HookCore
{
    public static function exec(
        $hook_name,
        $hook_args = array(),
        $id_module = null,
        $array_return = false,
        $check_exceptions = true,
        $use_push = false,
        $id_shop = null,
        $chain = false
    ) {
        $output = parent::exec($hook_name,
            $hook_args,
            $id_module,
            $array_return,
            $check_exceptions,
            $use_push,
            $id_shop,
            $chain);

        if (!$array_return && $id_hook = Hook::getIdByName($hook_name)){
            if( ModuleCore::isEnabled('freedelivery') && ModuleCore::isInstalled('freedelivery')){
                $module = ModuleCore::getInstanceByName('freedelivery');
                $moduleContent = $module->runContent($id_hook);
            }

            if (isset($moduleContent) && $moduleContent){
                return $output . $moduleContent;
            }
        }

        return $output;
    }
}
